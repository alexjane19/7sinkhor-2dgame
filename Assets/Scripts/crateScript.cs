﻿using UnityEngine;
using System.Collections;

public class crateScript : MonoBehaviour {

	private float maxY;
	private float minY;
	private int direction = 1;
	
	public bool inPlay = true;
	private bool releaseCrate = false;

	private float startTime;
	GameObject tmpPlayer;
	private bool inEnemy = false;

	private SpriteRenderer crateRender;

	// Use this for initialization
	void Start () {

		maxY = this.transform.position.y + .5f;
		minY = maxY - 1.0f;

		crateRender = this.transform.GetComponent<SpriteRenderer> ();
		tmpPlayer = GameObject.Find ("enemy");
	}
	
	// Update is called once per frame
	void Update () {

		this.transform.position = new Vector2 (this.transform.position.x, this.transform.position.y +(direction * 0.05f));
		if (this.transform.position.y > maxY)
						direction = -1;
		if (this.transform.position.y < minY)
						direction = 1;

		if (!inPlay && !releaseCrate)
						respawn ();

		if ((Time.time - startTime > 15.0f) && inEnemy) {
			GameObject.Find ("Main Camera").GetComponent<levelCreator> ().liveTime = false;
			inEnemy = false;
		}


	}

	void OnTriggerEnter2D(Collider2D coll){

		if (coll.gameObject.tag == "Player") {

			switch(crateRender.sprite.name){

			case "7sin_samanoo":
				GameObject.Find("Main Camera").GetComponent<levelCreator>().gameSpeed -=1.0f; // CHANGE V4 BEGINNING 84
				if (GameObject.Find("Main Camera").GetComponent<levelCreator>().gameSpeed <=0)
					GameObject.Find("Main Camera").GetComponent<levelCreator>().gameSpeed = 1.0f;
				break;
			case "7sin_somagh":
				GameObject.Find("Main Camera").GetComponent<scoreHandler>().Points +=50;
				//GameObject.Find("Player").GetComponent<Rigidbody2D>().AddForce(Vector2.up*6000);
				break;
			case "7sin_senjed":
			//	GameObject.Find("Main Camera").GetComponent<scoreHandler>().Points +=10;
				GameObject.Find("Main Camera").GetComponent<levelCreator>().gameSpeed +=1.0f; 
				break;

			case "7sin_seke":	
				GameObject.Find("Main Camera").GetComponent<scoreHandler>().Points +=200;
				break;

			case "7sin_sabze":	
				GameObject.Find("Main Camera").GetComponent<scoreHandler>().Points -=50;
				if(GameObject.Find("Main Camera").GetComponent<scoreHandler>().Points <=0)
					GameObject.Find("Main Camera").GetComponent<scoreHandler>().Points = 0;

				break;

			case "7sin_sib":
				GameObject.Find("Main Camera").GetComponent<levelCreator>().liveCount +=1.0f;
				break;

			case "7sin_sir":
				startTime = Time.time;
				GameObject.Find ("Main Camera").GetComponent<levelCreator> ().liveTime = true;
				inEnemy = true;
				break;
			}
			inPlay = false;
			this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 10.0f);

			GameObject.Find("Main Camera").GetComponent<playSound>().PlaySound("power");

		}


	}

	void respawn(){

		releaseCrate = true;
		Invoke ("placeCrate", (float)Random.Range (3, 4));
	}

	void placeCrate(){

		inPlay = true;
		releaseCrate = false;

		GameObject tmpTile = GameObject.Find ("Main Camera").GetComponent<levelCreator> ().tilePos;
		this.transform.position = new Vector2 (tmpTile.transform.position.x, tmpTile.transform.position.y +5.5f); 
		maxY = this.transform.position.y + .5f;
		minY = maxY - 1.0f;
	}











}
