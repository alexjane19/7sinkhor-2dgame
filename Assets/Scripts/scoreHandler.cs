﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
[ExecuteInEditMode]

public class scoreHandler : MonoBehaviour {

	private int _score = -6;
	private int _bestscore;
	private GameObject text_score;
	private GameObject text_bestscore ;
	// Use this for initialization
	void Start () {
		_bestscore = getHighScoreFromDb ();
		text_score = GameObject.Find ("t_score");
		text_bestscore = GameObject.Find ("t_bestscore");
		text_bestscore.GetComponent<Text> ().text = _bestscore.ToString ();

	}
	
	// Update is called once per frame
	void Update () {
		text_score.GetComponent<Text> ().text = _score.ToString ();
	}
/*
	void OnGUI(){
		GUI.color = Color.green;
		GUIStyle _style = GUI.skin.GetStyle("Label");
		_style.alignment = TextAnchor.UpperLeft;
		_style.fontSize = 20;
		GUI.Label(new Rect (20,20,200,200), _score.ToString(),_style);
		_style.alignment = TextAnchor.UpperRight;
		GUI.Label(new Rect (Screen.width - 220,20,200,200), "Highscore: "+ _bestscore.ToString(),_style);
	}

*/
	public int Points{
		get{return _score;}
		set{_score = value;}
	}

	static string Md5Sum(string s){

		s += GameObject.Find ("xxmd5").transform.GetChild (0).name;
		System.Security.Cryptography.MD5 h = System.Security.Cryptography.MD5.Create ();
		byte[] data = h.ComputeHash (System.Text.Encoding.Default.GetBytes(s));

		System.Text.StringBuilder sb = new System.Text.StringBuilder ();
		for (int i =0; i<data.Length; i++) {
			sb.Append(data[i].ToString("x2"));
		}
		return sb.ToString ();
	}

	public void saveVal(int val){
		string tmpV = Md5Sum (val.ToString ());
		PlayerPrefs.SetString ("score_hash", tmpV);
		PlayerPrefs.SetInt ("score", val);
	}

	private int dec(string val){
		int tmpV = 0;
		if (val == "") {
			saveVal(tmpV);
		
		}else{
		
			if(val.Equals(Md5Sum(PlayerPrefs.GetInt("score").ToString()))){
				tmpV = PlayerPrefs.GetInt("score");
			}else{
				saveVal(0);
			}
		
		}
		return tmpV;
	}


	private int getHighScoreFromDb(){
		return dec (PlayerPrefs.GetString("score_hash"));
	}

	public void sendToHighScore(){
		if (_score > _bestscore) {
			saveVal(_score);
		}
	}


}
