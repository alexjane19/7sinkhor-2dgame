﻿using UnityEngine;
using System.Collections;

public class playSound : MonoBehaviour {

	private AudioSource[] _audiSource;
	public bool _pause;
	public bool _mute_sound;
	public bool _mute_music;
	// Use this for initialization
	void Start () {
		_pause = false;
		_mute_sound = false;
		_mute_music = false;
		_audiSource = this.GetComponents<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void PlaySound(string type){

		switch (type) {

		case "jump":
			_audiSource[0].Play();
			break;
		case "power":
			_audiSource[1].Play();
			break;
		case "die":
			_audiSource[2].Play();
			break;
		case "restart":
			_audiSource[3].Play();
			break;
		}

		
	}

	public void pauseMusic()

	{
		_pause = ! _pause;
		if(_pause)
			_audiSource [4].Pause ();
		if (! _pause)
			_audiSource [4].Play();
	}

	public void muteSounds()
	{
		_mute_sound = ! _mute_sound;
		for(int i=0; i<4; i++)
			_audiSource [i].mute = _mute_sound;

	}

	
	public void muteMusic()
	{
		_mute_music = ! _mute_music;
		_audiSource [4].mute = _mute_music;
		
	}

	




}
